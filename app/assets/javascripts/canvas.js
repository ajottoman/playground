function canvasReSize() {
    $("#canvas").css("width",  $(this).height()+"px");
}
// Which HTML element is the target of the event
function mouseTarget(e) {
    var targ;
    if (!e) var e = window.event;
    if (e.target) targ = e.target;
    else if (e.srcElement) targ = e.srcElement;
    if (targ.nodeType == 3) // defeat Safari bug
        targ = targ.parentNode;
    return targ;
}

/*
*
* cW is current canvas Width
* cH is current canvas Height
* canvas here is the SVG model of the canvas
* relPosX and relPosY are relative co-ordinates of the mouse (0-1,0-1)
*
* */


$(document).ready(function () {
    var canvas =$("#canvas");
    var canvasSvg = SVG('canvas');
    var brushSize = $(".downText");


    function canvasAction(canvas,cW,cH, relPosX, relPosY) {
        var color = $(".sp-preview-inner")[0].style.backgroundColor;
        var rect = canvas.rect(Number(brushSize.text()), Number(brushSize.text())).attr({ fill: color});
    }

    canvas.click(function (e) {
        var canvasBound = canvas.get(0).getBoundingClientRect();
        //console.log(canvasBound);
        var cW = canvasBound.width,cH = canvasBound.height;

        var posX = $(this).offset().left, posY = $(this).offset().top;

        var relPosX = (e.pageX - posX)/cW, relPosY = (e.pageY - posY)/cH;
        //alert("(x,y):"+(e.pageX - posX)+ ' , ' + (e.pageY - posY)+" % "+relPosX+' , ' +relPosY);

        canvasAction(canvasSvg, relPosX, relPosY);

    });
    $(window).resize(function() {
        canvasReSize();
    });
    canvasReSize();
});