$(document).ready(function () {
    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
    });
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(150);
    });
    var xmlns = "http://www.w3.org/2000/svg",
        select = function (s) {
            return document.querySelector(s);
        },
        selectAll = function (s) {
            return document.querySelectorAll(s);
        },
        container = select('.container'),
        dragger = select('#dragger'),
        dragVal,
        maxDrag = 300

    //center the container cos it's pretty an' that
    TweenMax.set(container, {
        position: 'absolute',
        top: '50%',
        left: '50%',
        xPercent: -50,
        yPercent: -50
    })
    TweenMax.set('svg', {
        visibility: 'visible'
    })

    TweenMax.set('#upText', {
        alpha: 0,
        transformOrigin: '50% 50%'
    })

    TweenLite.defaultEase = Elastic.easeOut.config(0.4, 0.1);

    var tl = new TimelineMax({
        paused: true
    });
    tl.addLabel("blobUp")
        .to('#display', 1, {
            attr: {
                cy: '-=40',
                r: 30
            }
        })
        .to('#dragger', 1, {
            attr: {
                //cy:'-=2',
                r: 8
            }
        }, '-=1')
        .set('#dragger', {
            strokeWidth: 4
        }, '-=1')
        .to('.downText', 1, {
            //alpha:0,
            alpha: 0,
            transformOrigin: '50% 50%'
        }, '-=1')
        .to('.upText', 1, {
            //alpha:1,
            alpha: 1,
            transformOrigin: '50% 50%'
        }, '-=1')
        .addPause()
        .addLabel("blobDown")
        .to('#display', 1, {
            attr: {
                cy: 299.5,
                r: 0
            },
            ease: Expo.easeOut
        })
        .to('#dragger', 1, {
            attr: {
                //cy:'-=35',
                r: 15
            }
        }, '-=1')
        .set('#dragger', {
            strokeWidth: 0
        }, '-=1')
        .to('.downText', 1, {
            alpha: 1,
            ease: Power4.easeOut
        }, '-=1')
        .to('.upText', 0.2, {
            alpha: 0,
            ease: Power4.easeOut,
            attr: {
                y: '+=45'
            }
        }, '-=1')

    Draggable.create(dragger, {
        type: 'x',
        cursor: 'pointer',
        throwProps: true,
        bounds: {
            minX: 0,
            maxX: maxDrag
        },
        onPress: function () {
            $('.stroke-container').css("overflow", "visible");
            tl.play('blobUp')
        },
        onRelease: function () {

            tl.play('blobDown')
            $('.stroke-container').css("overflow", "none");
        },
        onDrag: dragUpdate,
        onThrowUpdate: dragUpdate
    })

    function dragUpdate() {
        dragVal = Math.round((this.target._gsTransform.x / maxDrag) * 100);
        select('.downText').textContent = select('.upText').textContent = dragVal;
        TweenMax.to('#display', 1.3, {
            x: this.target._gsTransform.x

        })

        TweenMax.staggerTo(['.upText', '.downText'], 1, {
            // x:this.target._gsTransform.x,
            cycle: {
                attr: [{
                    x: this.target._gsTransform.x + 146
                }]
            },
            ease: Elastic.easeOut.config(1, 0.4)
        }, '0.02')

    }

    TweenMax.to(dragger, 1, {
        x: 10,
        onUpdate: dragUpdate,
        ease: Power1.easeInOut
    })
    $("#colorpicker-primary").spectrum({
        showInput: true,
        showAlpha: true,
        allowEmpty: true,
        showPaletteOnly: true,
        togglePaletteOnly: true,
        togglePaletteMoreText: 'more',
        togglePaletteLessText: 'less',
        palette: [
            ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
            ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
            ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
            ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
            ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
            ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
            ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
            ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
        ]
    });



});