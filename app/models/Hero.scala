package models

import play.api.libs.json.Json


case class Hero(name: String)

object Hero {
  implicit val heroFormat = Json.format[Hero]
}