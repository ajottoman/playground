package controllers

import play.api.mvc._

class Application extends Controller {

  def index = Action {
    Ok(views.html.index("SVG Easel")("Here is the Easel"))

  }
}