
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/aj/IdeaProjects/playground/conf/routes
// @DATE:Mon Jun 26 12:24:01 SAST 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
