
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object index_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(content: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>

<html>
    <head>
        <title>"""),_display_(/*7.17*/title),format.raw/*7.22*/("""</title>
        <link rel="stylesheet" media="screen" href=""""),_display_(/*8.54*/routes/*8.60*/.Assets.at("stylesheets/main.css")),format.raw/*8.94*/("""">
        <link rel="shortcut icon" type="image/png" href=""""),_display_(/*9.59*/routes/*9.65*/.Assets.at("images/favicon.png")),format.raw/*9.97*/("""">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://unpkg.com/nanoreset/nanoreset.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <style>
                @media (max-width: 767px) """),format.raw/*15.44*/("""{"""),format.raw/*15.45*/("""
                """),format.raw/*16.17*/(""".navbar-nav .dropdown-menu """),format.raw/*16.44*/("""{"""),format.raw/*16.45*/("""
                    """),format.raw/*17.21*/("""position: static;
                    float: none;
                    width: 100%;
                    margin-top: 0;
                    background-color: transparent;
                    border: 0;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                """),format.raw/*25.17*/("""}"""),format.raw/*25.18*/("""
                """),format.raw/*26.17*/(""".navbar-nav .dropdown-menu > li > a """),format.raw/*26.53*/("""{"""),format.raw/*26.54*/("""
                    """),format.raw/*27.21*/("""line-height: 20px;
                    padding: 5px 15px 5px 25px;
                """),format.raw/*29.17*/("""}"""),format.raw/*29.18*/("""
                """),format.raw/*30.17*/(""".navbar-nav .dropdown-menu .dropdown-header """),format.raw/*30.61*/("""{"""),format.raw/*30.62*/("""
                    """),format.raw/*31.21*/("""padding: 5px 15px 5px 25px;
                """),format.raw/*32.17*/("""}"""),format.raw/*32.18*/("""

                """),format.raw/*34.17*/(""".navbar-default .navbar-nav .dropdown-menu > li > a """),format.raw/*34.69*/("""{"""),format.raw/*34.70*/("""
                    """),format.raw/*35.21*/("""color: #777;
                """),format.raw/*36.17*/("""}"""),format.raw/*36.18*/("""
                """),format.raw/*37.17*/("""}"""),format.raw/*37.18*/("""
        """),format.raw/*38.9*/("""</style>
        <style>
                body """),format.raw/*40.22*/("""{"""),format.raw/*40.23*/("""
                    """),format.raw/*41.21*/("""display: flex;
                    flex-direction: column;
                """),format.raw/*43.17*/("""}"""),format.raw/*43.18*/("""
                """),format.raw/*44.17*/(""".content """),format.raw/*44.26*/("""{"""),format.raw/*44.27*/("""
                    """),format.raw/*45.21*/("""display: flex;
                    flex-grow: 1;
                """),format.raw/*47.17*/("""}"""),format.raw/*47.18*/("""
                """),format.raw/*48.17*/(""".nav-flex """),format.raw/*48.27*/("""{"""),format.raw/*48.28*/("""
                    """),format.raw/*49.21*/("""display: flex;
                    margin-bottom: 0;
                """),format.raw/*51.17*/("""}"""),format.raw/*51.18*/("""
                """),format.raw/*52.17*/(""".left """),format.raw/*52.23*/("""{"""),format.raw/*52.24*/("""
                    """),format.raw/*53.21*/("""display: flex;
                    flex: 1;
                    flex-direction: column;
                """),format.raw/*56.17*/("""}"""),format.raw/*56.18*/("""
                """),format.raw/*57.17*/(""".right """),format.raw/*57.24*/("""{"""),format.raw/*57.25*/("""
                    """),format.raw/*58.21*/("""display: flex;
                    flex: 4;
                    flex-direction: column;
                """),format.raw/*61.17*/("""}"""),format.raw/*61.18*/("""
                """),format.raw/*62.17*/("""#data """),format.raw/*62.23*/("""{"""),format.raw/*62.24*/("""
                    """),format.raw/*63.21*/("""outline: inset 1px blue;
                    background: #dcfff5;

                    border-right: inset 10px lemonchiffon;
                    border-bottom: inset 15px lemonchiffon;
                    flex: 1.5;
                """),format.raw/*69.17*/("""}"""),format.raw/*69.18*/("""
                """),format.raw/*70.17*/("""#preview """),format.raw/*70.26*/("""{"""),format.raw/*70.27*/("""
                    """),format.raw/*71.21*/("""flex: 1;
                """),format.raw/*72.17*/("""}"""),format.raw/*72.18*/("""
                """),format.raw/*73.17*/("""#editor """),format.raw/*73.25*/("""{"""),format.raw/*73.26*/("""
                    """),format.raw/*74.21*/("""flex: 16.18;
                    border-bottom:solid 1px;
                    border-left:solid 1px;
                """),format.raw/*77.17*/("""}"""),format.raw/*77.18*/("""
                """),format.raw/*78.17*/("""#toolbar """),format.raw/*78.26*/("""{"""),format.raw/*78.27*/("""
                    """),format.raw/*79.21*/("""flex-shrink: 1;
                    background: #fffdf0;
                """),format.raw/*81.17*/("""}"""),format.raw/*81.18*/("""
                """),format.raw/*82.17*/(""".dropdown-menu """),format.raw/*82.32*/("""{"""),format.raw/*82.33*/("""
                    """),format.raw/*83.21*/("""top:auto;
                    bottom:100%;
                """),format.raw/*85.17*/("""}"""),format.raw/*85.18*/("""
        """),format.raw/*86.9*/("""</style>
    </head>
    <body>

        <div class="content">
            <div class="left">
                <section id="data">"""),_display_(/*92.37*/content),format.raw/*92.44*/("""</section>
                <section id="preview"></section>
            </div>
            <div class="right">
                <section id="editor"></section>
                <section id="toolbar">
                    <nav class="navbar navbar-default nav-flex">
                        <div class="container">
                                <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#">Easel</a>
                            </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">One more separated link</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </section>
            </div>
        </div>

    </body>
    <script>
            $( document ).ready( function() """),format.raw/*149.45*/("""{"""),format.raw/*149.46*/("""
                """),format.raw/*150.17*/("""$( '.dropdown' ).on( 'show.bs.dropdown', function() """),format.raw/*150.69*/("""{"""),format.raw/*150.70*/("""
                    """),format.raw/*151.21*/("""$( this ).find( '.dropdown-menu' ).first().stop( true, true ).slideDown( 150 );
                """),format.raw/*152.17*/("""}"""),format.raw/*152.18*/(""" """),format.raw/*152.19*/(""");
                $('.dropdown').on( 'hide.bs.dropdown', function()"""),format.raw/*153.66*/("""{"""),format.raw/*153.67*/("""
                    """),format.raw/*154.21*/("""$( this ).find( '.dropdown-menu' ).first().stop( true, true ).slideUp( 150 );
                """),format.raw/*155.17*/("""}"""),format.raw/*155.18*/(""" """),format.raw/*155.19*/(""");
            """),format.raw/*156.13*/("""}"""),format.raw/*156.14*/(""" """),format.raw/*156.15*/(""");
    </script>
</html>
"""))
      }
    }
  }

  def render(title:String,content:String): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (String) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}


}

/**/
object index extends index_Scope0.index
              /*
                  -- GENERATED --
                  DATE: Tue Jun 27 09:44:43 SAST 2017
                  SOURCE: /home/aj/IdeaProjects/playground/app/views/index.scala.html
                  HASH: fd5e633a113f567c66a75d74062fc07ddd2263cb
                  MATRIX: 534->1|661->33|689->35|766->86|791->91|879->153|893->159|947->193|1034->254|1048->260|1100->292|1627->792|1656->793|1701->810|1756->837|1785->838|1834->859|2163->1160|2192->1161|2237->1178|2301->1214|2330->1215|2379->1236|2490->1319|2519->1320|2564->1337|2636->1381|2665->1382|2714->1403|2786->1447|2815->1448|2861->1466|2941->1518|2970->1519|3019->1540|3076->1569|3105->1570|3150->1587|3179->1588|3215->1597|3289->1643|3318->1644|3367->1665|3470->1740|3499->1741|3544->1758|3581->1767|3610->1768|3659->1789|3752->1854|3781->1855|3826->1872|3864->1882|3893->1883|3942->1904|4039->1973|4068->1974|4113->1991|4147->1997|4176->1998|4225->2019|4357->2123|4386->2124|4431->2141|4466->2148|4495->2149|4544->2170|4676->2274|4705->2275|4750->2292|4784->2298|4813->2299|4862->2320|5123->2553|5152->2554|5197->2571|5234->2580|5263->2581|5312->2602|5365->2627|5394->2628|5439->2645|5475->2653|5504->2654|5553->2675|5698->2792|5727->2793|5772->2810|5809->2819|5838->2820|5887->2841|5988->2914|6017->2915|6062->2932|6105->2947|6134->2948|6183->2969|6270->3028|6299->3029|6335->3038|6492->3168|6520->3175|10307->6933|10337->6934|10383->6951|10464->7003|10494->7004|10544->7025|10669->7121|10699->7122|10729->7123|10826->7191|10856->7192|10906->7213|11029->7307|11059->7308|11089->7309|11133->7324|11163->7325|11193->7326
                  LINES: 20->1|25->1|27->3|31->7|31->7|32->8|32->8|32->8|33->9|33->9|33->9|39->15|39->15|40->16|40->16|40->16|41->17|49->25|49->25|50->26|50->26|50->26|51->27|53->29|53->29|54->30|54->30|54->30|55->31|56->32|56->32|58->34|58->34|58->34|59->35|60->36|60->36|61->37|61->37|62->38|64->40|64->40|65->41|67->43|67->43|68->44|68->44|68->44|69->45|71->47|71->47|72->48|72->48|72->48|73->49|75->51|75->51|76->52|76->52|76->52|77->53|80->56|80->56|81->57|81->57|81->57|82->58|85->61|85->61|86->62|86->62|86->62|87->63|93->69|93->69|94->70|94->70|94->70|95->71|96->72|96->72|97->73|97->73|97->73|98->74|101->77|101->77|102->78|102->78|102->78|103->79|105->81|105->81|106->82|106->82|106->82|107->83|109->85|109->85|110->86|116->92|116->92|173->149|173->149|174->150|174->150|174->150|175->151|176->152|176->152|176->152|177->153|177->153|178->154|179->155|179->155|179->155|180->156|180->156|180->156
                  -- GENERATED --
              */
          